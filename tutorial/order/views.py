import json

from django.shortcuts import render
import logging

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from tutorial.order import services

logger = logging.getLogger(__name__)


@api_view(["POST"])
def save_order(request):
    try:
        try:
            id = request.data['id']
        except Exception as e:
            message = 'ERROR Can not get param name id :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        try:
            user_id = request.data['user_id']
        except Exception as e:
            message = 'ERROR Can not get param name user_id :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        try:
            order_no = request.data['order_no']
        except Exception as e:
            message = 'ERROR Can not get param name order_no :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        try:
            order_date = request.data['order_date']
        except Exception as e:
            message = 'ERROR Can not get param name order_date :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        try:
            created_at = request.data['created_at']
        except Exception as e:

            message = 'ERROR Can not get param name created_at :' + str(e)
            logger.error(message)

            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        services.save_order(id=id, user_id=user_id, order_no=order_no, order_date=order_date, created_at=created_at)
        data = []
        success = True
        message = ''
    except Exception as e:
        success = False
        message = str(e)
        data = []
    return Response({'success': success, 'message': message, 'data': data})



@api_view(["DELETE"])
def remove_order(request, order_id=None):
    try:
        services.delete_order(id=order_id)
        data = []
        success = True
        message = ''
    except Exception as e:
        success = False
        message = str(e)
        data = []
    return Response({'success': success, 'message': message, 'data': data})


@api_view(["GET"])
def get_order(request, order_id=None):
    try:
        data = services.get_order(id=order_id)
        success = True
        message = ''
    except Exception as e:
        success = False
        message = str(e)
        data = []
    return Response({'success': success, 'message': message, 'data': data})

@api_view(["GET"])
def get_order_customer_id(request, order_id=None):
    try:
        data = services.get_order_customer_id(id=order_id)
        success = True
        message = ''

    except Exception as e:
        success = False
        message = str(e)
        data = []
    return Response({'success': success, 'message': message, 'data': data})