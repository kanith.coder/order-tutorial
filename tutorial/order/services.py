from django.db import transaction

from tutorial.order import daos


def save_order(id, user_id, order_no, order_date, created_at):
    with transaction.atomic():
        order = daos.save_order(id=id, user_id=user_id, order_no=order_no, order_date=order_date, created_at=created_at)
    return order


def delete_order(id):
    with transaction.atomic():
        daos.remove_order(id=id)


def get_order(id):
    return daos.get_order(id=id)


def get_order_customer_id(id):
    query_order_customer = daos.get_order_customer_id(id=id)

    print('query_order_customer=', query_order_customer)
    for order_customer in query_order_customer:

        data = order_customer['full_name']
        print("order_customer=", data)

    return data