import logging

from django.db import transaction
from django.db import connection
from tutorial.order.models import Order

logger = logging.getLogger(__name__)

def save_order(id, user_id, order_no, order_date, created_at):
    with transaction.atomic():
        try:
            order = Order.objects.select_for_update().filter(id=id)[0]
        except IndexError as e:
            logger.debug(' Can not found order :' + str(e))
            order = Order()
        order.user_id = user_id
        order.order_no = order_no
        order.order_date = order_date
        order.created_at = created_at
        order.save()
    return order


def remove_order(id):
    with transaction.atomic():
        try:
            order = Order.objects.get(id=id)
            order.delete()
        except Exception as e:
            logger.debug('ERROR Can not delete order :' + str(e))
            raise Exception('ERROR Can not delete order :' + str(e))


def get_order(id):
    try:
        order = Order.objects.get(id=id)
        data = {
            'id': order.id,
            'user_id': order.user_id,
            'order_no': order.order_no,
            'order_date': order.order_date,
            'created_at': order.created_at

        }
        return data
    except Exception as e:
        logger.debug('ERROR Can not get order :' + str(e))
        raise Exception('ERROR Can not get order :' + str(e))


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def get_order_customer_id(id=id):
    try:
        query = f'''
                 select full_name from customer_customer join order_order  on customer_customer.id = order_order.user_id 
                '''
        with connection.cursor() as cursor:
            cursor.execute(query)
            row = dictfetchall(cursor)
        return row

    except Exception as e:
        logger.debug('ERROR Can not get order :' + str(e))
        raise Exception('ERROR Can not get order :' + str(e))