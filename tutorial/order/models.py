from django.db import models

# Create your models here.


class Order(models.Model):
    # id is primarykey generate by django
    user_id = models.DecimalField(decimal_places=2, max_digits=18, default=0)
    order_no = models.CharField(max_length=2000, blank=True, null=True)
    order_date = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    order_customer_id = models.CharField(max_length=2000, blank=True, null=True)

