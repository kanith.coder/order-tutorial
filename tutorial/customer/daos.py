import logging

from django.db import transaction

from tutorial.customer.models import Customer

logger = logging.getLogger(__name__)

def save_customer(id, full_name, created_at):
    with transaction.atomic():
        try:
            customer = Customer.objects.select_for_update().filter(id=id)[0]
        except IndexError as e:
            logger.debug(' Can not found customer :' + str(e))
            customer = Customer()

        customer.full_name = full_name
        customer.created_at = created_at
        customer.save()
    return customer


def remove_customer(id):
    with transaction.atomic():
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
        except Exception as e:
            logger.debug('ERROR Can not delete customer :' + str(e))
            raise Exception('ERROR Can not delete customer :' + str(e))


def get_customer(id):
    try:
        customer = Customer.objects.get(id=id)
        data = {
            'id': customer.id,
            'full_name': customer.full_name,
            'created_at': customer.created_at
        }
        return data
    except Exception as e:
        logger.debug('ERROR Can not get customer :' + str(e))
        raise Exception('ERROR Can not get customer :' + str(e))
