from django.db import models

# Create your models here.


class Customer(models.Model):
    # id is primarykey generate by django
    full_name = models.CharField(max_length=2000, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
