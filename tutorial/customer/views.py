import json

from django.shortcuts import render
import logging

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from tutorial.customer import services

logger = logging.getLogger(__name__)


@api_view(["POST"])
def save_customer(request):
    try:
        try:
            id = request.data['id']
        except Exception as e:
            message = 'ERROR Can not get param name id :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        try:
            full_name = request.data['full_name']
        except Exception as e:
            message = 'ERROR Can not get param name full_name :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        try:
            created_at = request.data['created_at']
        except Exception as e:
            message = 'ERROR Can not get param name created_at :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        services.save_customer(id=id, full_name=full_name, created_at=created_at)
        data = []
        success = True
        message = ''
    except Exception as e:
        success = False
        message = str(e)
        data = []
    return Response({'success': success, 'message': message, 'data': data})


@api_view(["DELETE"])
def remove_customer(request, customer_id=None):
    try:
        services.delete_customer(id=customer_id)
        data = []
        success = True
        message = ''
    except Exception as e:
        success = False
        message = str(e)
        data = []
    return Response({'success': success, 'message': message, 'data': data})


@api_view(["GET"])
def get_customer(request, customer_id=None):
    try:
        data = services.get_customer(id=customer_id)
        success = True
        message = ''
    except Exception as e:
        success = False
        message = str(e)
        data = []
    return Response({'success': success, 'message': message, 'data': data})