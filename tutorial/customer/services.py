from django.db import transaction

from tutorial.customer import daos


def save_customer(id, full_name, created_at):
    with transaction.atomic():
        customer = daos.save_customer(id=id, full_name=full_name, created_at=created_at)
    return customer


def delete_customer(id):
    with transaction.atomic():
        daos.remove_customer(id=id)


def get_customer(id):
    return daos.get_customer(id=id)
