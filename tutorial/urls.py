"""tutorial URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from rest_framework import routers

from tutorial.customer.views import save_customer, remove_customer, get_customer
from tutorial.order.views import save_order, remove_order, get_order, get_order_customer_id
from tutorial.products.views import save_products, remove_products, get_products

router = routers.DefaultRouter(trailing_slash=False)

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^api/customer/save', save_customer),
    url(r'^api/customer/(?P<customer_id>.*)/remove', remove_customer),
    url(r'^api/customer/get/(?P<customer_id>.*)', get_customer),

    url(r'^api/order/save', save_order),
    url(r'^api/order/(?P<order_id>.*)/remove', remove_order),
    url(r'^api/order/get/(?P<order_id>.*)', get_order),
    url(r'^api/order_customer_id/get/(?P<order_id>.*)', get_order_customer_id),

    url(r'^api/products/save', save_products),
    url(r'^api/products/(?P<products_id>.*)/remove', remove_products),
    url(r'^api/products/get/(?P<products_id>.*)', get_products)



]
