from django.db import transaction

from tutorial.products import daos


def save_products(id, name, price, status, created_at):
    with transaction.atomic():
        products = daos.save_products(id=id, name=name, price=price, status=status, created_at=created_at)
    return products


def delete_products(id):
    with transaction.atomic():
        daos.remove_products(id=id)


def get_products(id):
    return daos.get_products(id=id)
