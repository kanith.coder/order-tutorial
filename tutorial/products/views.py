import json

from django.shortcuts import render
import logging

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from tutorial.products import services

logger = logging.getLogger(__name__)


@api_view(["POST"])
def save_products(request):
    try:
        try:
            id = request.data['id']
        except Exception as e:
            message = 'ERROR Can not get param name id :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        try:
            name = request.data['name']
        except Exception as e:
            message = 'ERROR Can not get param name name :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        try:
            price = request.data['price']
        except Exception as e:
            message = 'ERROR Can not get param name price :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        try:
            status = request.data['status']
        except Exception as e:
            message = 'ERROR Can not get param name status :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)


        try:
            created_at = request.data['created_at']
        except Exception as e:
            message = 'ERROR Can not get param name created_at :' + str(e)
            logger.error(message)
            return Response({'success': False, 'message': message, 'data': []}, status=status.HTTP_400_BAD_REQUEST)

        services.save_products(id=id, name=name, price=price, status=status, created_at=created_at)
        data = []
        success = True
        message = ''
    except Exception as e:
        success = False
        message = str(e)
        data = []
    return Response({'success': success, 'message': message, 'data': data})


@api_view(["DELETE"])
def remove_products(request, products_id=None):
    try:
        services.delete_products(id=products_id)
        data = []
        success = True
        message = ''
    except Exception as e:
        success = False
        message = str(e)
        data = []
    return Response({'success': success, 'message': message, 'data': data})


@api_view(["GET"])
def get_products(request, products_id=None):
    try:
        data = services.get_products(id=products_id)
        success = True
        message = ''
    except Exception as e:
        success = False
        message = str(e)
        data = []
    return Response({'success': success, 'message': message, 'data': data})

