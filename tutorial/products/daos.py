import logging

from django.db import transaction

from tutorial.products.models import Products

logger = logging.getLogger(__name__)

def save_products(id, name, price, status,  created_at):
    with transaction.atomic():
        try:
            products = Products.objects.select_for_update().filter(id=id)[0]
        except IndexError as e:
            logger.debug(' Can not found products :' + str(e))
            products = Products()

        products.name = name
        products.price = price
        products.status = status
        products.created_at = created_at
        products.save()
    return products


def remove_products(id):
    with transaction.atomic():
        try:
            products = Products.objects.get(id=id)
            products.delete()
        except Exception as e:
            logger.debug('ERROR Can not delete products :' + str(e))
            raise Exception('ERROR Can not delete products :' + str(e))


def get_products(id):
    try:
        products = Products.objects.get(id=id)
        data = {
            'id': products.id,
            'name': products.name,
            'price': products.price,
            'status': products.status,
            'created_at': products.created_at
        }
        return data
    except Exception as e:
        logger.debug('ERROR Can not get products :' + str(e))
        raise Exception('ERROR Can not get products :' + str(e))
