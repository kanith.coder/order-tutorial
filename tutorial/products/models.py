from django.db import models

# Create your models here.


class Products(models.Model):
    # id is primarykey generate by django
    name = models.CharField(max_length=2000, blank=True, null=True)
    price = models.DecimalField(decimal_places=2, max_digits=18, default=0)
    status = models.CharField(max_length=2000, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)