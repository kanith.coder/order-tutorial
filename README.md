# order-tutorial


# Setup Project
**1. Create a virtual environment to isolate our package dependencies locally**

`python3 -m venv env`

`source env/bin/activate`

**2. Install Django and Django REST framework into the virtual environment**

`pip install -r requirement.txt`

**3. Innitial Development Datebase (sqllite) access to your project folder (find manage.py file) **

`python manage.py migrate`

`python manage.py createsuperuser --email admin@example.com --username admin`


**4. Run Server**

`python manage.py runserver`

default page http://127.0.0.1:8000

for admin page http://127.0.0.1:8000/admin/



# Create Module App
`django-admin startapp [your app name]`

**register module**

`-> setting.py -> INSTALLED_APPS add [your app name]`


# Create Model (Create table)...

`python manage.py makemigrations`

`python manage.py migrate`